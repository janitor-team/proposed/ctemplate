Source: ctemplate
Section: libdevel
Priority: optional
Maintainer: Debian KDE Extras Team <pkg-kde-extras@lists.alioth.debian.org>
Uploaders: Mark Purcell <msp@debian.org>
Build-Depends: debhelper-compat (= 12),
 python3:any,
Standards-Version: 4.5.0
Vcs-Git: https://salsa.debian.org/qt-kde-team/3rdparty/ctemplate.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/3rdparty/ctemplate
Homepage: https://github.com/olafvdspek/ctemplate

Package: libctemplate-dev
Architecture: any
Depends: libctemplate3 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}, ${perl:Depends}
Description: Simple but powerful template language for C++ - development files
 This package contains a library implementing a simple but
 powerful template language for C++.  It emphasizes separating logic
 from presentation: it is impossible to embed application logic in this
 template language.  The devel package contains static and debug
 libraries and header files for developing applications that use the
 ctemplate package.
 This package contains the ctemplate development files.

Package: libctemplate3
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Simple but powerful template language for C++
 This package contains a library implementing a simple but
 powerful template language for C++.  It emphasizes separating logic
 from presentation: it is impossible to embed application logic in this
 template language.  This limits the power of the template language
 without limiting the power of the template *system*.  Indeed, Google's
 "main" web search uses this system exclusively for formatting output.
