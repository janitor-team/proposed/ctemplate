ctemplate (2.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update the patches:
    - upstream_Partial-Python-3-support.patch: drop, backported from upstream
    - upstream_Python-3-support.patch: drop, backported from upstream
    - switch-to-python3.diff: drop, fixed upstream
  * Bump Standards-Version to 4.5.0, no changes required.
  * Update the install path of the documentation directory, which is not
    versioned now.
  * Update the lintian overrides.

 -- Pino Toscano <pino@debian.org>  Sun, 08 Mar 2020 18:57:51 +0100

ctemplate (2.3-5) unstable; urgency=medium

  * Team upload.
  * Switch the helper Python build scripts to Python 3: (Closes: #942931)
    - backport upstream commit ce1b8fe617bc052c1d9bf0be6000ead2fd205c6b;
      patch upstream_Partial-Python-3-support.patch
    - backport part of upstream commit 4b7e6c52dc7cbb4d51c9abcebcbac91ec256a62b;
      patch upstream_Python-3-support.patch
    - change the shebangs to python3, and fix one more Python 3 issue; patch
      switch-to-python3.diff
    - switch the python:any build dependency to python3:any

 -- Pino Toscano <pino@debian.org>  Sun, 29 Dec 2019 12:50:44 +0100

ctemplate (2.3-4) unstable; urgency=medium

  * Team upload.
  * Switch Vcs-* fields to salsa.debian.org.
  * Add the configuration for the CI on salsa.
  * Bump the debhelper compatibility to 12:
    - switch the debhelper build dependency to debhelper-compat 12
    - remove debian/compat
    - remove --parallel for dh, as now done by default
    - drop the dh_makeshlibs override, as -V is done by default
    - stop explicitly using the autoreconf dh addon, as it is used by default
    - drop remove the dh-autoreconf build dependency
    - switch from dh_install to dh_missing for --list-missing
  * Switch from --list-missing to --fail-missing for dh_missing; everything is
    installed already.
  * Bump Standards-Version to 4.4.1, no changes required.
  * Remove trailing whitespaces in changelog.
  * Update the lintian overrides.
  * Drop patch gcc-4.6.patch, obsolete since version 1.0 already.
  * Convert copyright to copyright-format v1.0, as its format is basically that
    one already.
  * Change the python build dependency to python:any, as Python is needed only
    as build tool.
  * Remove Florian Reinhard from Uploaders, as he did not do any work after the
    initial release; thanks for your work!

 -- Pino Toscano <pino@debian.org>  Sat, 28 Dec 2019 12:54:00 +0100

ctemplate (2.3-3) unstable; urgency=medium

  * Team upload.
  * Upload to unstable. (See #823669)
  * Move removal of tests leftovers right after dh_auto_test execution.

 -- Pino Toscano <pino@debian.org>  Tue, 10 May 2016 19:39:14 +0200

ctemplate (2.3-2) experimental; urgency=medium

  * Team upload.
  * Run the tests without multiple parallel jobs, as it seems they conflict
    or depend on each other; also make the tests run fatal again.
  * Manually remove few tests-related directories in /tmp not cleaned up by
    the tests themselves.
  * Tie library dependencies to the latest upstream version.

 -- Pino Toscano <pino@debian.org>  Fri, 06 May 2016 21:22:29 +0200

ctemplate (2.3-1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Rename libctemplate2v5 to libctemplate3, according to the new SONAME.
  * Update the patches:
    - r129.patch: drop, backported from upstream
  * Convert the package to multiarch:
    - remove the --libdir setting
    - adapt the library paths in .install files
    - add "Multi-Arch: same" to libctemplate3
  * Remove the libctemplate2 breaks/replaces from libctemplate3, since there
    are no more file conflicts.
  * Add the python build dependency, needed during build.
  * Use ${perl:Depends} in libctemplate-dev.
  * Ship the pkg-config .pc files in libctemplate-dev.

 -- Pino Toscano <pino@debian.org>  Thu, 05 May 2016 00:01:49 +0200

ctemplate (2.2-6) unstable; urgency=medium

  * Team upload.
  * Upstream moved to github, so:
    - switch Homepage to https://github.com/olafvdspek/ctemplate, both
      in control and copyright
    - rewrite watch to scan tags on github
  * Improve installation of files into packages:
    - copy documentation files from the destdir, and not the source dir
    - explicitly remove from destdir all the files that must not be
      copied at all (libtool .la, READMEs and such)
    - pass --list-missing to dh_install, so files not copied are easily
      spotted
    - remove debian/not-installed, no more useful now
  * Bump debhelper compatibility to 9:
    - bump compat to 9
    - bump the debhelper build dependency to 9
    - force --libdir=/usr/lib for now, multiarch adaptation will come
      with the next SONAME break
  * Make the .install files a bit more specific (especially for the
    libraries), and sort them.
  * Refresh patches (just to simplify the diff header), and improve
    DEP3 headers.
  * Install NEWS as documentation in libctemplate-dev.
  * Update Vcs-Browser field.
  * Bump Standards-Version to 3.9.8, no changes required.
  * The rebuild will hopefully fix #797761. (Closes: #797761)

 -- Pino Toscano <pino@debian.org>  Wed, 04 May 2016 19:56:51 +0200

ctemplate (2.2-5) unstable; urgency=medium

  * Team upload.
  * Migrate to autoreconf. (Closes: #757992, #737368)
  * Rename lib package to libctemplate2v5, due to gcc5 api
    incompatibilities.
  * Ignore unittests random error.

 -- Maximiliano Curia <maxy@debian.org>  Fri, 07 Aug 2015 15:57:20 +0200

ctemplate (2.2-4) unstable; urgency=low

  * dh --with autotools_dev "config.{sub,guess} AArch64" (Closes: #727349)
  * Update Vcs: fields
  * Standards_Version: 3.9.4

 -- Mark Purcell <msp@debian.org>  Sat, 09 Nov 2013 14:12:53 +1100

ctemplate (2.2-3) unstable; urgency=low

  * Fix "g++-4.7 -std=c++0x issue" upstream r129.patch (Closes: #665360)

 -- Mark Purcell <msp@debian.org>  Wed, 20 Jun 2012 22:54:29 +1000

ctemplate (2.2-2) unstable; urgency=low

  * Upload to unstable - coordinated through debian-release

 -- Mark Purcell <msp@debian.org>  Sat, 19 May 2012 09:26:51 +1000

ctemplate (2.2-1) experimental; urgency=low

  * New upstream release
    - Fixes "ftbfs with GCC-4.7" (Closes: #667145)
    - Fixes "New Upsstream, Watch File" (Closes: #658404)
    - Fix "g++-4.7 -std=c++0x issue" (Closes: #665360)
  * --program-prefix=ctemplate-
  * NEW package libctemplate2 - match-soname

 -- Mark Purcell <msp@debian.org>  Sat, 19 May 2012 07:55:00 +1000

ctemplate (2.0-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sat, 18 Feb 2012 16:37:54 +1100

ctemplate (1.1-1) unstable; urgency=low

  * lca2012 release

  * New upstream release
  * fix debian/watch

 -- Mark Purcell <msp@debian.org>  Wed, 18 Jan 2012 15:52:34 +1100

ctemplate (1.0-1) unstable; urgency=low

  * New upstream release
    - Fixes "New Version: 1.0" (Closes: #642009)
  * Drop dbug592142-sparc.patch incorporated upstream 0.98

 -- Mark Purcell <msp@debian.org>  Sun, 09 Oct 2011 20:39:41 +1100

ctemplate (0.97-4) unstable; urgency=low

  * Ack NMU.  Thanks gregor.
  * Fixed description-synopsis-starts-with-article
  * debian/rules: export PTHREAD_CFLAGS=-lpthread
    - Fixes dpkg-shlibdeps: warning: symbol found in none of the libraries

 -- Mark Purcell <msp@debian.org>  Sun, 09 Oct 2011 19:34:10 +1100

ctemplate (0.97-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "FTBFS: ./src/ctemplate/template_dictionary.h:73:11: error:
    'ptrdiff_t' does not name a type":
    apply patch from Ubuntu / Matthias Klose:
    - Add missing includes to build with g++-4.6.
      Closes: #625002, LP: #771025.

 -- gregor herrmann <gregoa@debian.org>  Tue, 04 Oct 2011 17:24:25 +0200

ctemplate (0.97-3) unstable; urgency=low

  * Add sparc memory alignment patch from Jurij Smakov <jurij@wooyd.org>
    - Fixes "FTBFS on sparc: 10 of 20 tests failed" (Closes: #592142)

 -- Mark Purcell <msp@debian.org>  Tue, 10 Aug 2010 08:08:29 +1000

ctemplate (0.97-2) unstable; urgency=low

  * Include debian/* in copyright
  * Fix "Erroneus debian/watch file" Thks Angel (Closes: #583883)
  * Standards-Version: 3.9.1:
    - Switch to dh 7
    - debian/rules add Vcs-*
    - source/format -> 3.0 (quilt)
    - Add libctemplate-dev.doc-base
  * debian/rules enable --parallel

 -- Mark Purcell <msp@debian.org>  Sat, 07 Aug 2010 11:35:18 +1000

ctemplate (0.97-1) unstable; urgency=low

  * New upstream release

  * Initial Upload to Debian (Closes: #580717)
  * Maintainer: Debian KDE Extras Team - Add myself to Uploaders:
  * Add Homepage: http://code.google.com/p/google-ctemplate/
  * Update debian/watch

 -- Mark Purcell <msp@debian.org>  Sat, 08 May 2010 10:00:30 +1000

ctemplate (0.96-0ubuntu1) lucid; urgency=low

  * Initial release (LP: #507717)

 -- Florian Reinhard <florian.reinhard@googlemail.com>  Thu, 14 Jan 2010 22:31:32 +0100
